from typing import Annotated, Any

from fastapi import FastAPI, Body
from fastapi.params import Depends
from sqlalchemy.orm import Session

from acceptor.acceptor_entities import AcceptorQry
from acceptor.database import SessionLocal
from domain.ChartQry import ChartQry
import acceptor.crud as crud
app = FastAPI()

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/hello/{name}")
async def say_hello(name: str):
    return {"message": f"Hello {name}"}


@app.get("/biz/bill/bba/chart")
async def get_chart(name: str='1') -> list:
    charts = []
    for i in range(3):
        charts.append({"xAxis": str(i), "max": str(i**2)})
    return charts

@app.post("/biz/bill/bba/chart2")
async def create_item(item: ChartQry):
    return item


@app.post("/biz/bill/bba/chart3")
async def create_item(item: Annotated[Any, Body()]):
    return item


@app.post("/biz/bill/app/index/acceptor/list")
async def get_acceptor_list(qry: Annotated[AcceptorQry, Body()], db: Session = Depends(get_db)):
    acceptor_list = crud.select_seba_acceptor_list(db, qry)
    return acceptor_list
