from pydantic import BaseModel
from typing import Union


class AcceptorVO(BaseModel):
    acceptor: Union[str, None] = None
    supportedDiscountBank: Union[str, None] = None


class AcceptorQry(BaseModel):
    acceptor: Union[str, None] = None
    current: Union[int, None] = 1
    size: Union[int, None] = 20
