from sqlalchemy import text
from sqlalchemy.orm import Session

from acceptor.acceptor_entities import AcceptorQry, AcceptorVO


def select_seba_acceptor_list(db: Session, qry: AcceptorQry):
    sql = "select * from\
        (select distinct a.accept_type_code,\
            a.pr_bank_name                        acceptor,\
            a.bank_union_number,\
            group_concat(a.pr_discount_bank_name ORDER BY CONVERT(a.pr_discount_bank_name USING gbk) SEPARATOR '、') supportedDiscountBank \
            from seba_price_basic_acceptor a\
            where a.pr_bank_name is not null\
            group by a.pr_bank_name) t1\
        order by t1.accept_type_code is null,CONVERT(t1.acceptor USING gbk) asc"
    result = db.execute(text(sql))
    users = []
    for row in result:
        user = AcceptorVO(acceptor=row.acceptor, supportedDiscountBank=row.supportedDiscountBank)
        users.append(user)
    return users
