from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import mysql.connector

engine = create_engine("mysql+mysqlconnector://developer:xXpoXbld4qUZJHL9@10.10.64.127:3306/biz_bill_service", echo=True)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
