from datetime import datetime, date
from typing import Union

from pydantic import BaseModel


class ChartQry(BaseModel):
    acceptor: Union[str, None] = None
    deal_date: Union[date, None] = None
